//
//  MercadoLibreShoppingTests.swift
//  MercadoLibreShoppingTests
//
//  Created by Victor Hugo Gil Alejandría on 26/09/2018.
//  Copyright © 2018 Victor Hugo Gil Alejandría. All rights reserved.
//

import XCTest
import ReactiveKit
import Dispatch


@testable import MercadoLibreShopping

class MercadoLibreShoppingTests: XCTestCase {

    var transaction: Transaction!
    
    override func setUp() {
        transaction = Transaction(monto: 15, tarjeta: "visa", banco: "288", cuotas: "1 cuota de $ 15,00 ($ 15,00)")
    }

    override func tearDown() {
        transaction = nil
    }

    func testGetBanks() {
        /// Usando expectations podemos probar de forma eficiente los mètodos asincronos para recuperar la información desde los servidores de MercadoLibre
        let expectation = XCTestExpectation(description: "Recuperar los bancos desde el servidor de Mercadolibre")
        _ = MercadoLibreServices.getBanks(transaction: transaction).observeNext(with: { (cardIssuers) in
            /// Chequeo que la informaciòn haya llegado
            XCTAssertNotNil(cardIssuers, "Posible error de comunicación.")
            /// Cheque si llegaron todos los elementos que se esperan
            XCTAssert(cardIssuers.count == 33)
            let first_bank = cardIssuers[0]
            let last_bank = cardIssuers[32]
            /// Verifico que el primero es el elemento esperado
            XCTAssert(first_bank.name == "Tarjeta Shopping")
            XCTAssert(last_bank.name == "Otro")
            expectation.fulfill()
        })
        
        wait(for: [expectation], timeout: 5.0)
    }

    func testGetPaymentMethods() {
        let expectation = XCTestExpectation(description: "Recuperar los metodos de pago desde el servidor de Mercadolibre")
        _ = MercadoLibreServices.getPaymentMethods(paymentType: .CreditCard).observeNext(with: { (paymentMethods) in
            XCTAssertNotNil(paymentMethods, "Posible error de comunicación.")
            XCTAssert(paymentMethods.count == 13)
            let first_payment_method = paymentMethods[0]
            let last_payment_method = paymentMethods[12]
            XCTAssert(first_payment_method.name == "Visa")
            XCTAssert(last_payment_method.name == "CMR")
            expectation.fulfill()
        })
        
        wait(for: [expectation], timeout: 5.0)
    }

    func testGetInstallments() {
        let expectation = XCTestExpectation(description: "Recuperar la lista de cuotas de pago desde el servidor de Mercadolibre")
        _ = MercadoLibreServices.getInstallments(transaction: transaction).observeNext(with: { (installment) in
            XCTAssertNotNil(installment.payer_costs, "Posible error de comunicación.")
            XCTAssert(installment.payer_costs?.count == 5)
            let first_payer_cost = installment.payer_costs![0]
            let last_payer_cost = installment.payer_costs![4]
            XCTAssert(first_payer_cost.recommended_message == "1 cuota de $ 15,00 ($ 15,00)")
            XCTAssert(last_payer_cost.recommended_message == "12 cuotas de $ 2,05 ($ 24,56)")
            expectation.fulfill()
        })
        
        wait(for: [expectation], timeout: 5.0)
    }
}
