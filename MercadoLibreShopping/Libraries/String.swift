//
//  String.swift
//  IwannaFramework
//
//  Created by Victor Alejandria on 12/23/16.
//  Copyright © 2016 ISMCenter. All rights reserved.
//

import UIKit

public extension String {
    // To convert a base64 string into an image.
    public func toImage() -> UIImage {
        let dataDecode:NSData = NSData(base64Encoded: self, options:.ignoreUnknownCharacters)!
        return UIImage(data: dataDecode as Data)!
    }

}
