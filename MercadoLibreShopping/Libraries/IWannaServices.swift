//
//  Services.swift
//  IWCocoa
//
//  Created by Victor Alejandria on 1/20/17.
//  Copyright © 2017. All rights reserved.
//
import ReactiveKit
import ObjectMapper
import Alamofire
import AlamofireObjectMapper

public struct Services {
    
    enum ServicesError: Error {
        case serverNotConfigured
    }
    
    public struct ServerResponse {
        public var success: Bool
        public var response: JSON
    }
    

	/// Función interna para recuperar los datos del servidor a conectarse desde el UserDefaults de la aplicacion.
	///
	/// - Returns: Retorna un string con el servidor de producción o desarrollo.
	/// - Throws: Puede devolver una excepción si el UserDefaults no se ha configurado con los datos del servidor
	///	a utilizar.
	public static func server() throws -> String {
        if UserDefaults.standard.bool(forKey: "isDevelopment") {
            guard let server = UserDefaults.standard.string(forKey: "devServer") else { throw ServicesError.serverNotConfigured }
            return server
        } else {
            guard let server = UserDefaults.standard.string(forKey: "prodServer") else { throw ServicesError.serverNotConfigured }
            return server
        }
    }


    /// Metodo que permite la consulta de servicios REST usando Alamofire. Se debe configurar el servidor en el
	///	UserDefaults de la aplicación con los valore isDevelopment en verdadero o falso y los valores devServer
	///	y prodServer con los servidores de desarrollo y producción respectivamente.
    ///
    /// - Parameters:
    ///   - url: Endpoint final de conexión al servidor.
    ///   - parameters: parametros a enviar al endpoint en caso de ser necesarios.
    ///   - method: metodo POST, GET, PUSH, DELETE para ejecutar en el endpoint
    ///   - encoding: Formato de envio de la información, por defecto se envian en JSONEncoding.default
    ///   - headers: Headers a enviar en caso de ser necesarios, Authorization, etc.
    /// - Returns: Retorna un objeto Signal con un ServerResponse o un NSError en caso de haber algún error.
    /// - Throws: Puede devolver una excepción en caso de que el servidor no se haya configurado correctamente
	///	en el UserDefaults de la aplicación.
    public static func request(url: String, parameters: Parameters? = nil, method: HTTPMethod, encoding: ParameterEncoding = JSONEncoding.default, headers: HTTPHeaders) throws -> Signal<ServerResponse, NSError> {
        let server = try self.server()
        let responseObject = Signal<ServerResponse, NSError> { observer in
            _ = Alamofire.request("\(server)\(url)", method: method, parameters: parameters, encoding: encoding, headers: headers).responseJSON(completionHandler: { (response) in
                if response.result.isSuccess {
                    if (response.result.value != nil) {
                        observer.next(ServerResponse.init(success: true, response: JSON(response.result.value!)))
                    } else {
                        observer.next(ServerResponse.init(success: false, response: JSON([])))
                    }
                } else {
                    observer.failed(response.result.error! as NSError)
                }
                
                observer.completed()
            })
            
            return NonDisposable.instance
        }
        return responseObject
    }
}
