//
//  Services.swift
//  IWCocoa
//
//  Created by Victor Alejandria on 1/20/17.
//  Copyright © 2017. All rights reserved.
//
import ObjectMapper
import Alamofire
import AlamofireObjectMapper
import ReactiveKit

public struct Services {
    
    enum ServicesError: Error {
        case serverNotConfigured
    }
    
    public struct ServerResponse {
        public var success: Bool
        public var response: JSON
    }

	/// Función interna para recuperar los datos del servidor a conectarse.
	///
	/// - Returns: Retorna un string con el servidor de producción o desarrollo.
	/// - Throws: Puede devolver una excepción si el UserDefaults no se ha configurado con los datos del servidor
	///	a utilizar.
	public static func server() throws -> String {
        let config = Utilities.parseConfig()
        if config.IsDevelopment {
            guard let server = config.DevServer else { throw ServicesError.serverNotConfigured }
            return server
        } else {
            guard let server = config.ProdServer else { throw ServicesError.serverNotConfigured }
            return server
        }
    }

    /// Función interna para recuperar los datos del API a conectarse.
    ///
    /// - Returns: Retorna un string con el API Key de producción o desarrollo.
    /// - Throws: Puede devolver una excepción si el UserDefaults no se ha configurado con los datos a utilizar.
    public static func api() throws -> String {
        let config = Utilities.parseConfig()
        if config.IsDevelopment {
            guard let server = config.DevAPIKey else { throw ServicesError.serverNotConfigured }
            return server
        } else {
            guard let server = config.ProdAPIKey else { throw ServicesError.serverNotConfigured }
            return server
        }
    }
    
    /// Funcion que retorna un item unico de un tipo especificado.
    ///
    /// - Parameters:
    ///   - url: URL del endpoint a consultar.
    ///   - parameters: Diccionario de parametros a pasar al servicio.
    ///   - method: Metodo HTTP a utilizar, puedes ser GET, POST, PUT, etc.
    ///   - encoding: Codificacion usada por los parametros.
    ///   - headers: HTTP Headers requeridos por la solicitud al servicio.
    ///   - returnType: Modelo que debe retornar el servicio, este debe ser del tipo Mappable
    /// - Returns: Retorna un objeto unico del tipo Mappable.
    static func request<T: Mappable>(url: String, parameters: Parameters? = nil, method: HTTPMethod, encoding: ParameterEncoding = JSONEncoding.default, headers: HTTPHeaders, returnType: T) -> Signal<(Bool, T), NSError> {
        let responseObject = Signal<(Bool, T), NSError> { observer in
            URLCache.shared.removeAllCachedResponses()
            let server = try! self.server()
            _ = Alamofire.request("\(server)\(url)", method: method, parameters: parameters, encoding: encoding, headers: headers).debugLog().responseString(completionHandler: { response in
                if response.result.isSuccess {
                    let responseModel = Mapper<T>().map(JSONString: response.result.value!)
                    observer.next((true, responseModel!))
                } else {
                    observer.failed(response.result.error! as NSError)
                }
                
                observer.completed()
            })
            
            return NonDisposable.instance
        }
        
        return responseObject
    }
 
    /// Funcion que retorna un arreglos de objetos de un tipo especificado.
    ///
    /// - Parameters:
    ///   - url: URL del endpoint a consultar.
    ///   - parameters: Diccionario de parametros a pasar al servicio.
    ///   - method: Metodo HTTP a utilizar, puedes ser GET, POST, PUT, etc.
    ///   - encoding: Codificacion usada por los parametros.
    ///   - headers: HTTP Headers requeridos por la solicitud al servicio.
    ///   - returnType: Modelo que debe retornar el servicio, este debe ser del tipo Mappable
    /// - Returns: Retorna un arreglo de objetos del tipo Mappable.
    static func requestArray<T: Mappable>(url: String, parameters: [String: Any]? = nil, method: HTTPMethod, encoding: ParameterEncoding = JSONEncoding.default, headers: HTTPHeaders, returnType: [T]) -> Signal<(Bool, [T]), NSError> {
        let responseObject = Signal<(Bool, [T]), NSError> { observer in
            let server = try! self.server()
            URLCache.shared.removeAllCachedResponses()
            _ = Alamofire.request("\(server)\(url)", method: method, parameters: parameters, encoding: encoding, headers: headers).debugLog().responseArray(completionHandler: { (response: DataResponse<[T]>) in
                if response.result.isSuccess {
                    observer.next((true, response.result.value!))
                } else {
                    observer.failed(response.result.error! as NSError)
                }
                
                observer.completed()
            })
            
            return NonDisposable.instance
        }
        
        return responseObject
    }
}
