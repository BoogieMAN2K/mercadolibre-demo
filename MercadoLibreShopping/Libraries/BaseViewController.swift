//
//  BaseViewController.swift
//  IwannaFramework
//
//  Created by Victor Alejandria on 9/5/17.
//  Copyright © 2017 Victor Alejandria. All rights reserved.
//

import UIKit

open class BaseViewController: UIViewController, UITextFieldDelegate {

	/// UIScrollView que esta en el UIViewController y que será usado para mover los controles en pantalla.
	open var internalScrollView: UIScrollView!

	/// UITextField en donde esta ubicado el teclado actualmente.
	open var activeField: UITextField!

}
