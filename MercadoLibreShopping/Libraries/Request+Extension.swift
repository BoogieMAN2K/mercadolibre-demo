//
//  Request+Extension.swift
//  MercadoLibreShopping
//
//  Created by Victor Hugo Gil Alejandría on 28/09/2018.
//  Copyright © 2018 Victor Hugo Gil Alejandría. All rights reserved.
//
import Alamofire

extension Request {
    public func debugLog() -> Self {
        #if DEBUG
        debugPrint("=======================================")
        debugPrint(self)
        debugPrint("=======================================")
        #endif
        return self
    }
}
