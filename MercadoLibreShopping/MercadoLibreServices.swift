//
//  MercadoLibreServices.swift
//  MercadoLibreShopping
//
//  Created by Victor Hugo Gil Alejandría on 26/09/2018.
//  Copyright © 2018 Victor Hugo Gil Alejandría. All rights reserved.
//

import Foundation
import ReactiveKit
import ObjectMapper

class MercadoLibreServices: NSObject {
    
    
    /// Recupera los metodos de pago disponibles en MercadoLibre para un tipo de pago especifico.
    ///
    /// - Parameter paymentType: Tipo de pago del cual se quieren recuperan las tarjetas disponibles.
    /// - Returns: Una lista de tipos de tarjetas disponibles para el pago seleccionado.
    static func getPaymentMethods(paymentType: PaymentTypes) -> Signal<[PaymentMethod], NSError> {
        let public_key = try! Services.api()
        let parameters = ["public_key": public_key] as [String: Any]
        
        let paymentSignal = Services.requestArray(url: "payment_methods", parameters: parameters, method: .get, headers:  ["Content-Type": "application/json"], returnType: [PaymentMethod()])
        
        return paymentSignal.map {
            $0.1.filter { $0.paymentTypeID == paymentType.rawValue }
        }
    }
    
    
    /// Recupera una lista de bancos para el método de pago indicado dentro del objeto Transaction
    ///
    /// - Parameter transaction: Objeto transaction con la información de la operacion a procesar.
    /// - Returns: Un objeto Signal con la lista de bancos que proveen la tarjeta.
    static func getBanks(transaction: Transaction) -> Signal<[CardIssuer], NSError> {
        let public_key = try! Services.api()
        let parameters = ["public_key": public_key, "payment_method_id": transaction.tarjeta] as [String: Any]
        
        let bankSignal = Services.requestArray(url: "payment_methods/card_issuers", parameters: parameters, method: .get, headers:  ["Content-Type": "application/json"], returnType: [CardIssuer()])
        
        return bankSignal.map {
            $0.1
        }
    }

    
    /// Recupera las cuotas que se pueden seleccionar dependiendo de los datos de la transaccion enviada.
    ///
    /// - Parameter transaction: Objeto transaction con la información de la operacion a procesar.
    /// - Returns: Un objeto Signal con la información de las cuotas a las cuales se puede procesar una operaciòn.
    static func getInstallments(transaction: Transaction) -> Signal<Installment, NSError> {
        let public_key = try! Services.api()
        let parameters = ["public_key": public_key, "amount": transaction.monto, "payment_method_id": transaction.tarjeta, "issuer.id": transaction.banco] as [String: Any]
        
        let installmentSignal = Services.request(url: "payment_methods/installments", parameters: parameters, method: .get, headers:  ["Content-Type": "application/json"], returnType: Installment())
        
        return installmentSignal.map { $0.1 }
    }

}
