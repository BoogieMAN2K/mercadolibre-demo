//
//  CardIssuer.swift
//  MercadoLibreShopping
//
//  Created by Victor Hugo Gil Alejandría on 26/09/2018.
//  Copyright © 2018 Victor Hugo Gil Alejandría. All rights reserved.
//
import Foundation
import ReactiveKit
import ObjectMapper

struct CardIssuer: Mappable {
    var id, name: String!
    var secureThumbnail: String!
    var thumbnail: String!
    var processingMode: String!
    
    init() {
    }
    
    init?(map: Map) {
        //Required
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        secureThumbnail <- map["secure_thumbnail"]
        thumbnail <- map["thumbnail"]
        processingMode <- map["processing_mode"]
    }

}
