//
//  PaymentMethod.swift
//  MercadoLibreShopping
//
//  Created by Victor Hugo Gil Alejandría on 26/09/2018.
//  Copyright © 2018 Victor Hugo Gil Alejandría. All rights reserved.
//

import Foundation
import ReactiveKit
import ObjectMapper

struct PaymentMethod: Mappable {
    var id, name, paymentTypeID, status: String!
    var secureThumbnail: String!
    var thumbnail: String!
    var deferredCapture: String!
    var settings: [Setting]!
    var additionalInfoNeeded: [String]!
    var minAllowedAmount, maxAllowedAmount, accreditationTime: Int!
    var financialInstitutions: [JSONAny]!
    var processingModes: [String]!
    
    init() {
    }
    
    init?(map: Map) {
        //Required
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        paymentTypeID <- map["payment_type_id"]
        status <- map["status"]
        secureThumbnail <- map["secure_thumbnail"]
        thumbnail <- map["thumbnail"]
        deferredCapture <- map["deferred_capture"]
        settings <- map["settings"]
        additionalInfoNeeded <- map["additional_info"]
        minAllowedAmount <- map["min_allowed_amount"]
        maxAllowedAmount <- map["max_allowed_amount"]
        accreditationTime <- map["accreditation_time"]
        financialInstitutions <- map["financial_institutions"]
        processingModes <- map ["processing_modes"]
    }
}

enum PaymentTypes : String {
    case CreditCard = "credit_card"
    case DebitCard = "debit_card"
    case Ticket = "ticket"
}

struct Setting: Mappable {
    var cardNumber: CardNumber!
    var bin: Bin!
    var securityCode: SecurityCode!
    
    init() {
    }
    
    init?(map: Map) {
        //Required
    }
    
    mutating func mapping(map: Map) {
        cardNumber <- map["card_number"]
        bin <- map["bin"]
        securityCode <- map["security_code"]
    }
}

struct Bin: Mappable {
    var pattern, installmentsPattern, exclusionPattern: String!
    
    init() {
    }
    
    init?(map: Map) {
        //Required
    }
    
    mutating func mapping(map: Map) {
        pattern <- map["pattern"]
        installmentsPattern <- map["installments_pattern"]
        exclusionPattern <- map["exclusion_pattern"]
    }
}

struct CardNumber: Mappable {
    var validation: String!
    var length: Int!
    
    init() {
    }
    
    init?(map: Map) {
        //Required
    }
    
    mutating func mapping(map: Map) {
        validation <- map["validation"]
        length <- map["length"]
    }
}

struct SecurityCode: Mappable {
    var length: Int!
    var cardLocation, mode: String!
    
    init() {
    }
    
    init?(map: Map) {
        //Required
    }
    
    mutating func mapping(map: Map) {
        length <- map["length"]
        cardLocation <- map["card_location"]
        mode <- map["mode"]
    }
}
