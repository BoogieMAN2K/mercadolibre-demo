//
//  Config.swift
//  MercadoLibreShopping
//
//  Created by Victor Hugo Gil Alejandría on 26/09/2018.
//  Copyright © 2018 Victor Hugo Gil Alejandría. All rights reserved.
//

public struct Config: Decodable {
    public enum CodingKeys: String, CodingKey {
        case IsDevelopment, ProdServer, ProdAPIKey, DevServer, DevAPIKey
    }
    
    let IsDevelopment: Bool
    let ProdServer: String?
    let ProdAPIKey: String?
    let DevServer: String?
    let DevAPIKey: String?
}
