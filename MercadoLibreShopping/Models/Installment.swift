//
//  Installment.swift
//  MercadoLibreShopping
//
//  Created by Victor Hugo Gil Alejandría on 26/09/2018.
//  Copyright © 2018 Victor Hugo Gil Alejandría. All rights reserved.

import Foundation
import ObjectMapper

struct Installment : Mappable {
    var payment_method_id : String?
    var payment_type_id : String?
    var issuer : Issuer?
    var processing_mode : String?
    var merchant_account_id : String?
    var payer_costs : [Payer_Costs]?
    
    init() {
    }
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        payment_method_id <- map["payment_method_id"]
        payment_type_id <- map["payment_type_id"]
        issuer <- map["issuer"]
        processing_mode <- map["processing_mode"]
        merchant_account_id <- map["merchant_account_id"]
        payer_costs <- map["payer_costs"]
    }
    
}

struct Issuer : Mappable {
    var id : String?
    var name : String?
    var secure_thumbnail : String?
    var thumbnail : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        secure_thumbnail <- map["secure_thumbnail"]
        thumbnail <- map["thumbnail"]
    }
    
}

struct Payer_Costs : Mappable {
    var installments : Int?
    var installment_rate : Int?
    var discount_rate : Int?
    var labels : [String]?
    var installment_rate_collector : [String]?
    var min_allowed_amount : Int?
    var max_allowed_amount : Int?
    var recommended_message : String?
    var installment_amount : Int?
    var total_amount : Int?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        installments <- map["installments"]
        installment_rate <- map["installment_rate"]
        discount_rate <- map["discount_rate"]
        labels <- map["labels"]
        installment_rate_collector <- map["installment_rate_collector"]
        min_allowed_amount <- map["min_allowed_amount"]
        max_allowed_amount <- map["max_allowed_amount"]
        recommended_message <- map["recommended_message"]
        installment_amount <- map["installment_amount"]
        total_amount <- map["total_amount"]
    }
    
}
