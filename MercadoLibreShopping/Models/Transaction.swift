//
//  Transaction.swift
//  MercadoLibreShopping
//
//  Created by Victor Hugo Gil Alejandría on 28/09/2018.
//  Copyright © 2018 Victor Hugo Gil Alejandría. All rights reserved.
//

public struct Transaction {
    var monto: Double
    var tarjeta: String
    var banco: String
    var cuotas: String
}
