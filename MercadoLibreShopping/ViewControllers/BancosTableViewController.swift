//
//  BancosTableViewController.swift
//  MercadoLibreShopping
//
//  Created by Victor Hugo Gil Alejandría on 28/09/2018.
//  Copyright © 2018 Victor Hugo Gil Alejandría. All rights reserved.
//

import UIKit
import ReactiveKit
import Bond
import AlamofireImage

class BancosTableViewController: UITableViewController {

    @IBOutlet var listaBancos: UITableView!
    var transaction = Transaction(monto: 0, tarjeta: "", banco: "", cuotas: "")
    var bancos: [CardIssuer]!
    var row: IndexPath!

    override func viewWillAppear(_ animated: Bool) {
        let banks = MutableObservableArray<CardIssuer>()
        _ = MercadoLibreServices.getBanks(transaction: transaction).observeNext { bancos in
            bancos.forEachEnumerated {(index, banco) in
                banks.append(banco)
            }
            self.bancos = bancos
            banks.bind(to: self.listaBancos, animated: true) { dataSource, indexPath, tableView in
                let cell = tableView.dequeueReusableCell(withIdentifier: "ElementCell", for: indexPath)
                    as! ElementsTableViewCell
                cell.name.text = dataSource[indexPath.row].name
                cell.imageItem.af_setImage(withURL: URL(string: dataSource[indexPath.row].secureThumbnail)!)
                return cell
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if row != nil {
            listaBancos.cellForRow(at: row)?.accessoryType = .none
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        listaBancos.cellForRow(at: indexPath)?.accessoryType = .checkmark
        self.transaction.banco = self.bancos[indexPath.row].id
        self.row = indexPath

        performSegue(withIdentifier: "ShowInstallments", sender: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let viewController = segue.destination as? InstallmentsTableViewController
        viewController?.transaction = self.transaction
    }

}
