//
//  FormaPagoViewController.swift
//  MercadoLibreShopping
//
//  Created by Victor Hugo Gil Alejandría on 28/09/2018.
//  Copyright © 2018 Victor Hugo Gil Alejandría. All rights reserved.
//

import UIKit
import ReactiveKit
import Bond
import AlamofireImage

class FormaPagoTableViewController: UITableViewController {

    @IBOutlet var listaPagos: UITableView!
    var transaction = Transaction(monto: 0, tarjeta: "", banco: "", cuotas: "")
    var metodosPago: [PaymentMethod]!
    var row: IndexPath!

    override func viewWillAppear(_ animated: Bool) {
        let payment = MutableObservableArray<PaymentMethod>()
        _ = MercadoLibreServices.getPaymentMethods(paymentType: .CreditCard).observeNext { paymentMethods in
            paymentMethods.forEachEnumerated { (index, paymentMethod) in
                payment.append(paymentMethod)
            }
            self.metodosPago = paymentMethods
            payment.bind(to: self.listaPagos, animated: true) { dataSource, indexPath, tableView in
                let cell = tableView.dequeueReusableCell(withIdentifier: "ElementCell", for: indexPath)
                    as! ElementsTableViewCell
                cell.name.text = dataSource[indexPath.row].name
                cell.imageItem.af_setImage(withURL: URL(string: dataSource[indexPath.row].secureThumbnail)!)
                return cell
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if row != nil {
            listaPagos.cellForRow(at: row)?.accessoryType = .none
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        listaPagos.cellForRow(at: indexPath)?.accessoryType = .checkmark
        self.transaction.tarjeta = self.metodosPago[indexPath.row].id
        self.row = indexPath
        performSegue(withIdentifier: "ShowBanks", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let viewController = segue.destination as? BancosTableViewController
        viewController?.transaction = self.transaction
    }

}
