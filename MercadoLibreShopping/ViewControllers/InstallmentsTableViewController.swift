//
//  InstallmentsTableViewController.swift
//  MercadoLibreShopping
//
//  Created by Victor Hugo Gil Alejandría on 28/09/2018.
//  Copyright © 2018 Victor Hugo Gil Alejandría. All rights reserved.
//

import UIKit
import ReactiveKit
import Bond
import AlamofireImage

class InstallmentsTableViewController: UITableViewController {

    @IBOutlet var listaCuotas: UITableView!
    var transaction = Transaction(monto: 0, tarjeta: "", banco: "", cuotas: "")
    var cuota: Installment!

    override func viewWillAppear(_ animated: Bool) {
        _ = MercadoLibreServices.getInstallments(transaction: transaction).observeNext { installment in
            let cuotas = MutableObservableArray<Payer_Costs>()
            if installment.payer_costs != nil {
                installment.payer_costs!.forEachEnumerated { (index, payerCost) in
                    cuotas.append(payerCost)
                }
                cuotas.bind(to: self.listaCuotas, animated: true) { dataSource, indexPath, tableView in
                    let cell = tableView.dequeueReusableCell(withIdentifier: "ElementCell", for: indexPath)
                        as! ElementsTableViewCell
                    cell.name.text = dataSource[indexPath.row].recommended_message
                    return cell
                }
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        listaCuotas.cellForRow(at: indexPath)?.accessoryType = .checkmark
        self.transaction.cuotas = self.cuota.payer_costs![indexPath.row].recommended_message ?? ""
        Utilities.alertMessage(viewController: self, title: "Resultado", message: "Monto: \(transaction.monto) \n Forma Pago: \(transaction.tarjeta) \n Codigo Banco: \(transaction.banco) \n Cuotas: \(transaction.cuotas)")
        performSegue(withIdentifier: "GoToHome", sender: self)
    }
}
