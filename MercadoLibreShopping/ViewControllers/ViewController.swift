//
//  ViewController.swift
//  MercadoLibreShopping
//
//  Created by Victor Hugo Gil Alejandría on 26/09/2018.
//  Copyright © 2018 Victor Hugo Gil Alejandría. All rights reserved.
//

import UIKit
import ReactiveKit
import Bond

class ViewController: BaseViewController {

    @IBOutlet var monto: UITextField!
    @IBOutlet var continuar: UIButton!
    
    let montoActual = Property("")
    var transaction = Transaction(monto: 0, tarjeta: "", banco: "", cuotas: "")

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Asigno el boton a la funcion seleccionarMetodoPago
        continuar.addTarget(self, action: #selector(seleccionarMetodoPago), for: .touchUpInside)
        // Esto me permite desactivar el boton de continuar si el campo de monto esta vacio...
        _ = monto.reactive.text.observeNext(with: {value in
            self.continuar.isEnabled = !(value?.isEmpty ?? false) && (Double(value ?? "") ?? 0) > 0
            // Guardo en transaction.monto el valor de lo que escriba el usuario si le puedo convertir a numero sino, coloco cero.
            self.transaction.monto = Double(value ?? "") ?? 0
        })
    }
    
    @objc func seleccionarMetodoPago() {
        performSegue(withIdentifier: "ShowPaymentMethod", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
            let viewController = segue.destination as? FormaPagoTableViewController
            viewController?.transaction = self.transaction
    }
}

